import EventHandler from '../events.js';
import { mapValues } from '../lodash.js';
import internal from './internal.js';

import { delegate } from "../../../../src/utility/delegates"


export default class Base extends EventHandler {
  
  bridge: any;
  interfaceName: string;
  id: any;

  constructor() {
    super();

    // Create the instance's `internal` property.
    this[internal] = Object.create(this[internal], {
      props: {
        enumerable: true,
        value: mapValues(this[internal].props, type => type.defaultValue())
      }
    });
  }


  // Dispatch an event from this instance across the bridge.
  dispatch(type, value) {
    this.bridge.notifyAsync(this.interfaceName + ':!', [this.id, type, value]);
  }
}




Object.defineProperties(Base, 
  Object.getOwnPropertyDescriptors(EventHandler.prototype))

Base.prototype[internal] = Object.create(Object.prototype);
