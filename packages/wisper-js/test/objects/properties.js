import assert from 'assert';
import { properties, Local } from '../../lib/objects.js';
import { string, number } from '../../lib/types.js';

describe('properties', () => {

  class A extends Local {}

  properties({
    name: string
  })( A );


  it('throws if an inherited property is redeclared', () => {
    assert.throws(() => {
      class B extends A {}

      properties({
        name: number
      })( B );

      return new B();
    }, /Can't redefine inherited property 'name: string'/ );
  });
});
