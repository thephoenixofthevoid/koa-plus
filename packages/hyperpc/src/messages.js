// This file is auto generated by the protocol-buffers compiler

/* eslint-disable quotes */
/* eslint-disable indent */
/* eslint-disable no-redeclare */
/* eslint-disable camelcase */

// Remember to `npm install --save protocol-buffers-encodings`
var encodings = require('protocol-buffers-encodings')
var varint = encodings.varint
var skip = encodings.skip

exports.TYPE = {
  MANIFEST: 1,
  CALL: 2,
  RETURN: 3
}

exports.CALL = {
  API: 1,
  OBJECT: 2
}

exports.RETURN = {
  CALLBACK: 1,
  PROMISE: 2
}

exports.PROMISE = {
  RESOLVE: 0,
  REJECT: 1
}

exports.ARGUMENT = {
  BYTES: 1,
  JSON: 2,
  CALLBACK: 3,
  RPCIFIED: 4,
  STREAM: 5,
  ERROR: 6
}

exports.STREAM = {
  READABLE: 1,
  WRITEABLE: 2,
  DUPLEX: 3
}

var Arg = exports.Arg = {
  buffer: true,
  encodingLength: null,
  encode: null,
  decode: null
}

var Call = exports.Call = {
  buffer: true,
  encodingLength: null,
  encode: null,
  decode: null
}

var Return = exports.Return = {
  buffer: true,
  encodingLength: null,
  encode: null,
  decode: null
}

var Manifest = exports.Manifest = {
  buffer: true,
  encodingLength: null,
  encode: null,
  decode: null
}

var Msg = exports.Msg = {
  buffer: true,
  encodingLength: null,
  encode: null,
  decode: null
}

defineArg()
defineCall()
defineReturn()
defineManifest()
defineMsg()

function defineArg () {
  var Rpcified = Arg.Rpcified = {
    buffer: true,
    encodingLength: null,
    encode: null,
    decode: null
  }

  var Stream = Arg.Stream = {
    buffer: true,
    encodingLength: null,
    encode: null,
    decode: null
  }

  defineRpcified()
  defineStream()

  function defineRpcified () {
    Rpcified.encodingLength = encodingLength
    Rpcified.encode = encode
    Rpcified.decode = decode

    function encodingLength (obj) {
      var length = 0
      if (!defined(obj.manifest)) throw new Error("manifest is required")
      var len = encodings.string.encodingLength(obj.manifest)
      length += 1 + len
      return length
    }

    function encode (obj, buf, offset) {
      if (!offset) offset = 0
      if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
      var oldOffset = offset
      if (!defined(obj.manifest)) throw new Error("manifest is required")
      buf[offset++] = 10
      encodings.string.encode(obj.manifest, buf, offset)
      offset += encodings.string.encode.bytes
      encode.bytes = offset - oldOffset
      return buf
    }

    function decode (buf, offset, end) {
      if (!offset) offset = 0
      if (!end) end = buf.length
      if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
      var oldOffset = offset
      var obj = {
        manifest: ""
      }
      var found0 = false
      while (true) {
        if (end <= offset) {
          if (!found0) throw new Error("Decoded message is not valid")
          decode.bytes = offset - oldOffset
          return obj
        }
        var prefix = varint.decode(buf, offset)
        offset += varint.decode.bytes
        var tag = prefix >> 3
        switch (tag) {
          case 1:
          obj.manifest = encodings.string.decode(buf, offset)
          offset += encodings.string.decode.bytes
          found0 = true
          break
          default:
          offset = skip(prefix & 7, buf, offset)
        }
      }
    }
  }

  function defineStream () {
    Stream.encodingLength = encodingLength
    Stream.encode = encode
    Stream.decode = decode

    function encodingLength (obj) {
      var length = 0
      if (!defined(obj.type)) throw new Error("type is required")
      var len = encodings.enum.encodingLength(obj.type)
      length += 1 + len
      if (!defined(obj.objectMode)) throw new Error("objectMode is required")
      var len = encodings.bool.encodingLength(obj.objectMode)
      length += 1 + len
      return length
    }

    function encode (obj, buf, offset) {
      if (!offset) offset = 0
      if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
      var oldOffset = offset
      if (!defined(obj.type)) throw new Error("type is required")
      buf[offset++] = 8
      encodings.enum.encode(obj.type, buf, offset)
      offset += encodings.enum.encode.bytes
      if (!defined(obj.objectMode)) throw new Error("objectMode is required")
      buf[offset++] = 16
      encodings.bool.encode(obj.objectMode, buf, offset)
      offset += encodings.bool.encode.bytes
      encode.bytes = offset - oldOffset
      return buf
    }

    function decode (buf, offset, end) {
      if (!offset) offset = 0
      if (!end) end = buf.length
      if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
      var oldOffset = offset
      var obj = {
        type: 1,
        objectMode: false
      }
      var found0 = false
      var found1 = false
      while (true) {
        if (end <= offset) {
          if (!found0 || !found1) throw new Error("Decoded message is not valid")
          decode.bytes = offset - oldOffset
          return obj
        }
        var prefix = varint.decode(buf, offset)
        offset += varint.decode.bytes
        var tag = prefix >> 3
        switch (tag) {
          case 1:
          obj.type = encodings.enum.decode(buf, offset)
          offset += encodings.enum.decode.bytes
          found0 = true
          break
          case 2:
          obj.objectMode = encodings.bool.decode(buf, offset)
          offset += encodings.bool.decode.bytes
          found1 = true
          break
          default:
          offset = skip(prefix & 7, buf, offset)
        }
      }
    }
  }

  Arg.encodingLength = encodingLength
  Arg.encode = encode
  Arg.decode = decode

  function encodingLength (obj) {
    var length = 0
    if ((+defined(obj.bytes) + +defined(obj.json) + +defined(obj.callback) + +defined(obj.rpcified) + +defined(obj.stream) + +defined(obj.error)) > 1) throw new Error("only one of the properties defined in oneof payload can be set")
    if (!defined(obj.type)) throw new Error("type is required")
    var len = encodings.enum.encodingLength(obj.type)
    length += 1 + len
    if (defined(obj.bytes)) {
      var len = encodings.bytes.encodingLength(obj.bytes)
      length += 1 + len
    }
    if (defined(obj.json)) {
      var len = encodings.string.encodingLength(obj.json)
      length += 1 + len
    }
    if (defined(obj.callback)) {
      var len = encodings.string.encodingLength(obj.callback)
      length += 1 + len
    }
    if (defined(obj.rpcified)) {
      var len = Rpcified.encodingLength(obj.rpcified)
      length += varint.encodingLength(len)
      length += 1 + len
    }
    if (defined(obj.stream)) {
      var len = Stream.encodingLength(obj.stream)
      length += varint.encodingLength(len)
      length += 1 + len
    }
    if (defined(obj.error)) {
      var len = encodings.string.encodingLength(obj.error)
      length += 1 + len
    }
    return length
  }

  function encode (obj, buf, offset) {
    if (!offset) offset = 0
    if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
    var oldOffset = offset
    if ((+defined(obj.bytes) + +defined(obj.json) + +defined(obj.callback) + +defined(obj.rpcified) + +defined(obj.stream) + +defined(obj.error)) > 1) throw new Error("only one of the properties defined in oneof payload can be set")
    if (!defined(obj.type)) throw new Error("type is required")
    buf[offset++] = 8
    encodings.enum.encode(obj.type, buf, offset)
    offset += encodings.enum.encode.bytes
    if (defined(obj.bytes)) {
      buf[offset++] = 18
      encodings.bytes.encode(obj.bytes, buf, offset)
      offset += encodings.bytes.encode.bytes
    }
    if (defined(obj.json)) {
      buf[offset++] = 26
      encodings.string.encode(obj.json, buf, offset)
      offset += encodings.string.encode.bytes
    }
    if (defined(obj.callback)) {
      buf[offset++] = 34
      encodings.string.encode(obj.callback, buf, offset)
      offset += encodings.string.encode.bytes
    }
    if (defined(obj.rpcified)) {
      buf[offset++] = 42
      varint.encode(Rpcified.encodingLength(obj.rpcified), buf, offset)
      offset += varint.encode.bytes
      Rpcified.encode(obj.rpcified, buf, offset)
      offset += Rpcified.encode.bytes
    }
    if (defined(obj.stream)) {
      buf[offset++] = 50
      varint.encode(Stream.encodingLength(obj.stream), buf, offset)
      offset += varint.encode.bytes
      Stream.encode(obj.stream, buf, offset)
      offset += Stream.encode.bytes
    }
    if (defined(obj.error)) {
      buf[offset++] = 58
      encodings.string.encode(obj.error, buf, offset)
      offset += encodings.string.encode.bytes
    }
    encode.bytes = offset - oldOffset
    return buf
  }

  function decode (buf, offset, end) {
    if (!offset) offset = 0
    if (!end) end = buf.length
    if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
    var oldOffset = offset
    var obj = {
      type: 1,
      bytes: null,
      json: "",
      callback: "",
      rpcified: null,
      stream: null,
      error: ""
    }
    var found0 = false
    while (true) {
      if (end <= offset) {
        if (!found0) throw new Error("Decoded message is not valid")
        decode.bytes = offset - oldOffset
        return obj
      }
      var prefix = varint.decode(buf, offset)
      offset += varint.decode.bytes
      var tag = prefix >> 3
      switch (tag) {
        case 1:
        obj.type = encodings.enum.decode(buf, offset)
        offset += encodings.enum.decode.bytes
        found0 = true
        break
        case 2:
        delete obj.json
        delete obj.callback
        delete obj.rpcified
        delete obj.stream
        delete obj.error
        obj.bytes = encodings.bytes.decode(buf, offset)
        offset += encodings.bytes.decode.bytes
        break
        case 3:
        delete obj.bytes
        delete obj.callback
        delete obj.rpcified
        delete obj.stream
        delete obj.error
        obj.json = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        case 4:
        delete obj.bytes
        delete obj.json
        delete obj.rpcified
        delete obj.stream
        delete obj.error
        obj.callback = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        case 5:
        delete obj.bytes
        delete obj.json
        delete obj.callback
        delete obj.stream
        delete obj.error
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.rpcified = Rpcified.decode(buf, offset, offset + len)
        offset += Rpcified.decode.bytes
        break
        case 6:
        delete obj.bytes
        delete obj.json
        delete obj.callback
        delete obj.rpcified
        delete obj.error
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.stream = Stream.decode(buf, offset, offset + len)
        offset += Stream.decode.bytes
        break
        case 7:
        delete obj.bytes
        delete obj.json
        delete obj.callback
        delete obj.rpcified
        delete obj.stream
        obj.error = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        default:
        offset = skip(prefix & 7, buf, offset)
      }
    }
  }
}

function defineCall () {
  Call.encodingLength = encodingLength
  Call.encode = encode
  Call.decode = decode

  function encodingLength (obj) {
    var length = 0
    if (!defined(obj.type)) throw new Error("type is required")
    var len = encodings.enum.encodingLength(obj.type)
    length += 1 + len
    if (!defined(obj.id)) throw new Error("id is required")
    var len = encodings.string.encodingLength(obj.id)
    length += 1 + len
    if (defined(obj.name)) {
      var len = encodings.string.encodingLength(obj.name)
      length += 1 + len
    }
    if (defined(obj.objectid)) {
      var len = encodings.string.encodingLength(obj.objectid)
      length += 1 + len
    }
    if (defined(obj.method)) {
      var len = encodings.string.encodingLength(obj.method)
      length += 1 + len
    }
    if (defined(obj.args)) {
      for (var i = 0; i < obj.args.length; i++) {
        if (!defined(obj.args[i])) continue
        var len = Arg.encodingLength(obj.args[i])
        length += varint.encodingLength(len)
        length += 1 + len
      }
    }
    return length
  }

  function encode (obj, buf, offset) {
    if (!offset) offset = 0
    if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
    var oldOffset = offset
    if (!defined(obj.type)) throw new Error("type is required")
    buf[offset++] = 8
    encodings.enum.encode(obj.type, buf, offset)
    offset += encodings.enum.encode.bytes
    if (!defined(obj.id)) throw new Error("id is required")
    buf[offset++] = 18
    encodings.string.encode(obj.id, buf, offset)
    offset += encodings.string.encode.bytes
    if (defined(obj.name)) {
      buf[offset++] = 26
      encodings.string.encode(obj.name, buf, offset)
      offset += encodings.string.encode.bytes
    }
    if (defined(obj.objectid)) {
      buf[offset++] = 34
      encodings.string.encode(obj.objectid, buf, offset)
      offset += encodings.string.encode.bytes
    }
    if (defined(obj.method)) {
      buf[offset++] = 42
      encodings.string.encode(obj.method, buf, offset)
      offset += encodings.string.encode.bytes
    }
    if (defined(obj.args)) {
      for (var i = 0; i < obj.args.length; i++) {
        if (!defined(obj.args[i])) continue
        buf[offset++] = 50
        varint.encode(Arg.encodingLength(obj.args[i]), buf, offset)
        offset += varint.encode.bytes
        Arg.encode(obj.args[i], buf, offset)
        offset += Arg.encode.bytes
      }
    }
    encode.bytes = offset - oldOffset
    return buf
  }

  function decode (buf, offset, end) {
    if (!offset) offset = 0
    if (!end) end = buf.length
    if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
    var oldOffset = offset
    var obj = {
      type: 1,
      id: "",
      name: "",
      objectid: "",
      method: "",
      args: []
    }
    var found0 = false
    var found1 = false
    while (true) {
      if (end <= offset) {
        if (!found0 || !found1) throw new Error("Decoded message is not valid")
        decode.bytes = offset - oldOffset
        return obj
      }
      var prefix = varint.decode(buf, offset)
      offset += varint.decode.bytes
      var tag = prefix >> 3
      switch (tag) {
        case 1:
        obj.type = encodings.enum.decode(buf, offset)
        offset += encodings.enum.decode.bytes
        found0 = true
        break
        case 2:
        obj.id = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        found1 = true
        break
        case 3:
        obj.name = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        case 4:
        obj.objectid = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        case 5:
        obj.method = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        break
        case 6:
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.args.push(Arg.decode(buf, offset, offset + len))
        offset += Arg.decode.bytes
        break
        default:
        offset = skip(prefix & 7, buf, offset)
      }
    }
  }
}

function defineReturn () {
  Return.encodingLength = encodingLength
  Return.encode = encode
  Return.decode = decode

  function encodingLength (obj) {
    var length = 0
    if (!defined(obj.type)) throw new Error("type is required")
    var len = encodings.enum.encodingLength(obj.type)
    length += 1 + len
    if (!defined(obj.id)) throw new Error("id is required")
    var len = encodings.string.encodingLength(obj.id)
    length += 1 + len
    if (defined(obj.promise)) {
      var len = encodings.enum.encodingLength(obj.promise)
      length += 1 + len
    }
    if (defined(obj.args)) {
      for (var i = 0; i < obj.args.length; i++) {
        if (!defined(obj.args[i])) continue
        var len = Arg.encodingLength(obj.args[i])
        length += varint.encodingLength(len)
        length += 1 + len
      }
    }
    return length
  }

  function encode (obj, buf, offset) {
    if (!offset) offset = 0
    if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
    var oldOffset = offset
    if (!defined(obj.type)) throw new Error("type is required")
    buf[offset++] = 8
    encodings.enum.encode(obj.type, buf, offset)
    offset += encodings.enum.encode.bytes
    if (!defined(obj.id)) throw new Error("id is required")
    buf[offset++] = 18
    encodings.string.encode(obj.id, buf, offset)
    offset += encodings.string.encode.bytes
    if (defined(obj.promise)) {
      buf[offset++] = 24
      encodings.enum.encode(obj.promise, buf, offset)
      offset += encodings.enum.encode.bytes
    }
    if (defined(obj.args)) {
      for (var i = 0; i < obj.args.length; i++) {
        if (!defined(obj.args[i])) continue
        buf[offset++] = 34
        varint.encode(Arg.encodingLength(obj.args[i]), buf, offset)
        offset += varint.encode.bytes
        Arg.encode(obj.args[i], buf, offset)
        offset += Arg.encode.bytes
      }
    }
    encode.bytes = offset - oldOffset
    return buf
  }

  function decode (buf, offset, end) {
    if (!offset) offset = 0
    if (!end) end = buf.length
    if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
    var oldOffset = offset
    var obj = {
      type: 1,
      id: "",
      promise: 0,
      args: []
    }
    var found0 = false
    var found1 = false
    while (true) {
      if (end <= offset) {
        if (!found0 || !found1) throw new Error("Decoded message is not valid")
        decode.bytes = offset - oldOffset
        return obj
      }
      var prefix = varint.decode(buf, offset)
      offset += varint.decode.bytes
      var tag = prefix >> 3
      switch (tag) {
        case 1:
        obj.type = encodings.enum.decode(buf, offset)
        offset += encodings.enum.decode.bytes
        found0 = true
        break
        case 2:
        obj.id = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        found1 = true
        break
        case 3:
        obj.promise = encodings.enum.decode(buf, offset)
        offset += encodings.enum.decode.bytes
        break
        case 4:
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.args.push(Arg.decode(buf, offset, offset + len))
        offset += Arg.decode.bytes
        break
        default:
        offset = skip(prefix & 7, buf, offset)
      }
    }
  }
}

function defineManifest () {
  Manifest.encodingLength = encodingLength
  Manifest.encode = encode
  Manifest.decode = decode

  function encodingLength (obj) {
    var length = 0
    if (!defined(obj.manifest)) throw new Error("manifest is required")
    var len = encodings.string.encodingLength(obj.manifest)
    length += 1 + len
    if (!defined(obj.nonce)) throw new Error("nonce is required")
    var len = encodings.varint.encodingLength(obj.nonce)
    length += 1 + len
    return length
  }

  function encode (obj, buf, offset) {
    if (!offset) offset = 0
    if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
    var oldOffset = offset
    if (!defined(obj.manifest)) throw new Error("manifest is required")
    buf[offset++] = 10
    encodings.string.encode(obj.manifest, buf, offset)
    offset += encodings.string.encode.bytes
    if (!defined(obj.nonce)) throw new Error("nonce is required")
    buf[offset++] = 16
    encodings.varint.encode(obj.nonce, buf, offset)
    offset += encodings.varint.encode.bytes
    encode.bytes = offset - oldOffset
    return buf
  }

  function decode (buf, offset, end) {
    if (!offset) offset = 0
    if (!end) end = buf.length
    if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
    var oldOffset = offset
    var obj = {
      manifest: "",
      nonce: 0
    }
    var found0 = false
    var found1 = false
    while (true) {
      if (end <= offset) {
        if (!found0 || !found1) throw new Error("Decoded message is not valid")
        decode.bytes = offset - oldOffset
        return obj
      }
      var prefix = varint.decode(buf, offset)
      offset += varint.decode.bytes
      var tag = prefix >> 3
      switch (tag) {
        case 1:
        obj.manifest = encodings.string.decode(buf, offset)
        offset += encodings.string.decode.bytes
        found0 = true
        break
        case 2:
        obj.nonce = encodings.varint.decode(buf, offset)
        offset += encodings.varint.decode.bytes
        found1 = true
        break
        default:
        offset = skip(prefix & 7, buf, offset)
      }
    }
  }
}

function defineMsg () {
  Msg.encodingLength = encodingLength
  Msg.encode = encode
  Msg.decode = decode

  function encodingLength (obj) {
    var length = 0
    if ((+defined(obj.manifest) + +defined(obj.call) + +defined(obj.return)) > 1) throw new Error("only one of the properties defined in oneof msg can be set")
    if (!defined(obj.type)) throw new Error("type is required")
    var len = encodings.enum.encodingLength(obj.type)
    length += 1 + len
    if (defined(obj.manifest)) {
      var len = Manifest.encodingLength(obj.manifest)
      length += varint.encodingLength(len)
      length += 1 + len
    }
    if (defined(obj.call)) {
      var len = Call.encodingLength(obj.call)
      length += varint.encodingLength(len)
      length += 1 + len
    }
    if (defined(obj.return)) {
      var len = Return.encodingLength(obj.return)
      length += varint.encodingLength(len)
      length += 1 + len
    }
    return length
  }

  function encode (obj, buf, offset) {
    if (!offset) offset = 0
    if (!buf) buf = Buffer.allocUnsafe(encodingLength(obj))
    var oldOffset = offset
    if ((+defined(obj.manifest) + +defined(obj.call) + +defined(obj.return)) > 1) throw new Error("only one of the properties defined in oneof msg can be set")
    if (!defined(obj.type)) throw new Error("type is required")
    buf[offset++] = 8
    encodings.enum.encode(obj.type, buf, offset)
    offset += encodings.enum.encode.bytes
    if (defined(obj.manifest)) {
      buf[offset++] = 18
      varint.encode(Manifest.encodingLength(obj.manifest), buf, offset)
      offset += varint.encode.bytes
      Manifest.encode(obj.manifest, buf, offset)
      offset += Manifest.encode.bytes
    }
    if (defined(obj.call)) {
      buf[offset++] = 26
      varint.encode(Call.encodingLength(obj.call), buf, offset)
      offset += varint.encode.bytes
      Call.encode(obj.call, buf, offset)
      offset += Call.encode.bytes
    }
    if (defined(obj.return)) {
      buf[offset++] = 34
      varint.encode(Return.encodingLength(obj.return), buf, offset)
      offset += varint.encode.bytes
      Return.encode(obj.return, buf, offset)
      offset += Return.encode.bytes
    }
    encode.bytes = offset - oldOffset
    return buf
  }

  function decode (buf, offset, end) {
    if (!offset) offset = 0
    if (!end) end = buf.length
    if (!(end <= buf.length && offset <= buf.length)) throw new Error("Decoded message is not valid")
    var oldOffset = offset
    var obj = {
      type: 1,
      manifest: null,
      call: null,
      return: null
    }
    var found0 = false
    while (true) {
      if (end <= offset) {
        if (!found0) throw new Error("Decoded message is not valid")
        decode.bytes = offset - oldOffset
        return obj
      }
      var prefix = varint.decode(buf, offset)
      offset += varint.decode.bytes
      var tag = prefix >> 3
      switch (tag) {
        case 1:
        obj.type = encodings.enum.decode(buf, offset)
        offset += encodings.enum.decode.bytes
        found0 = true
        break
        case 2:
        delete obj.call
        delete obj.return
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.manifest = Manifest.decode(buf, offset, offset + len)
        offset += Manifest.decode.bytes
        break
        case 3:
        delete obj.manifest
        delete obj.return
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.call = Call.decode(buf, offset, offset + len)
        offset += Call.decode.bytes
        break
        case 4:
        delete obj.manifest
        delete obj.call
        var len = varint.decode(buf, offset)
        offset += varint.decode.bytes
        obj.return = Return.decode(buf, offset, offset + len)
        offset += Return.decode.bytes
        break
        default:
        offset = skip(prefix & 7, buf, offset)
      }
    }
  }
}

function defined (val) {
  return val !== null && val !== undefined && (typeof val !== 'number' || !isNaN(val))
}
