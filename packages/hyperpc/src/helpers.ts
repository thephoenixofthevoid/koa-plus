import { SEPERATOR, READABLE, WRITABLE } from "./constants";
import through from 'through2'
import {RPCify} from './rpcify.js';
import stream from 'stream'


// Pure helpers.
export function joinIds(...ids) {
    return ids.join(SEPERATOR)
}

export function calculatePrefix(nonce, remoteNonce) {
    if (remoteNonce > nonce) return 'A'
    else if (remoteNonce < nonce) return 'B'
    else return 'X' + (Math.round(Math.random() * 1000))
}

export function isFunc(obj) {
    return typeof obj === 'function'
}

export function isError(arg) {
    return arg instanceof Error
}

export function isRpcified(arg) {
    return arg instanceof RPCify
}

export function isPromise(obj) {
    return !!obj && (typeof obj === 'object' || typeof obj === 'function') && typeof obj.then === 'function'
}

export function isStream(obj) {
    return obj instanceof stream.Stream || (isObject(obj) && obj && (obj._readableState || obj._writableState))
}

export function isReadable(obj) {
    return isStream(obj) && typeof obj._read === 'function' && typeof obj._readableState === 'object'
}

export function isWritable(obj) {
    return isStream(obj) && typeof obj._write === 'function' && typeof obj._writableState === 'object'
}

export function isTransform(obj) {
    return isStream(obj) && typeof obj._transform === 'function' && typeof obj._transformState === 'object'
}

export function isObjectStream(stream) {
    if (isWritable(stream)) return stream._writableState.objectMode
    if (isReadable(stream)) return stream._readableState.objectMode
}

export function isBuffer(buf) {
    return Buffer.isBuffer(buf)
}

export function isObject(obj) {
    return (typeof obj === 'object')
}

export function isLiteral(val) {
    return (typeof val === 'boolean' || typeof val === 'string' || typeof val === 'number')
}

export function streamType(stream) {
    var type = 0

    // Special handling for transform streams. If it has no pipes attached,
    // assume its readable. Otherwise, assume its writable. If this leads
    // to unexpected behaviors, set up a duplex stream with duplexify and
    // use either setReadable() or setWritable() to only set up one end.
    if (isTransform(stream)) {
        if (typeof stream._readableState === 'object' && !stream._readableState.pipes) {
            return READABLE
        } else {
            return WRITABLE
        }
    }

    if (isReadable(stream)) type = type | READABLE
    if (isWritable(stream)) type = type | WRITABLE

    return type
}

export function pass(objectMode) {
    return through({ objectMode })
}

export function toObj() {
    return through.obj(function (chunk, enc, next) {
        this.push(JSON.parse(chunk))
        next()
    })
}

export function toBin() {
    return through.obj(function (chunk, enc, next) {
        this.push(JSON.stringify(chunk))
        next()
    })
}

export function maybeConvert(oneInObjMode, twoInObjMode) {
    if (oneInObjMode && !twoInObjMode) return toBin()
    if (!oneInObjMode && twoInObjMode) return toObj()
    if (oneInObjMode && twoInObjMode) return pass(true)
    if (!oneInObjMode && !twoInObjMode) return pass(false)
}
