function lookupRpcProperties(obj) {
    if (obj.__hyperpc) {
        return obj.__hyperpc;
    }
    if (obj.prototype && obj.prototype.__hyperpc) {
        return obj.prototype.__hyperpc;
    }
    obj = Object.getPrototypeOf(obj)
    if (!obj) return {}
    return lookupRpcProperties(obj)
}


interface PropertiyFilteringOptions {
    include?: string[];
    exclude?: string[];
    skipPrivate: boolean;
}

function isPrivate(name: string) {
    return name.startsWith("_")
}

function getPropertiesFilter<T extends PropertiyFilteringOptions>(o: PropertiyFilteringOptions) {
    const allowlist  = new Set(o.include);
    const rejectlist = new Set(o.exclude);
    rejectlist.add("constructor");

    return function(name: string): boolean {
        if (o.skipPrivate && isPrivate(name)) return false;
        if (rejectlist.has(name)) return false;
        if (!o.include) return true;
        return allowlist.has(name)
    }
}


function getName(obj: any) {
    if (obj.prototype) {
        return obj.name;
    } else {
        return Object.getPrototypeOf(obj).name
    }
}




function getMethodList(obj: any, options: PropertiyFilteringOptions) {
    if (obj.prototype) obj = obj.prototype;
    const unfiltered = [...getAllFuncs(obj) ]
    const filter = getPropertiesFilter(options);
    return unfiltered.filter(filter)
}





export class RPCify {

    opts: any;
    access: any;
    override: any;
    factory: any;
    instance: any;
    name: any;
    methods: any;
    cache: Map<any, any>;

    constructor(obj, opts: any = {}) {


        this.opts = Object.assign({
            skipPrivate: true,
            override: {},
            factory: null,
            access() { return true }
        }, lookupRpcProperties(obj), opts)

        this.access = this.opts.access
        this.override = this.opts.override
        this.name = getName(obj);

        if (obj.prototype) {
            // 1. Class (prototype)
            this.factory = opts.factory || makeDefaultFactory(obj)
            this.instance = null
            this.cache = new Map()
        } else {
            // 2. Object instance
            this.factory = null
            this.instance = obj
        }

        this.methods = getMethodList(obj, this.opts);
    }

    toManifest() {
        return {
            name: this.name,
            methods: this.methods
        }
    }

    makeNew(id, args) {
        console.log('makeNew', id, args, this.instance)
        if (this.instance) {
            return this.instance
        } else {
            const instance = this.factory(...args)
            this.cache.set(id, instance)
        }
    }

    makeCall(method, id, args) {
        let instance =  this.getInstance(id)
        if (!instance) return null;
        if (!this.access(instance, method, args)) return null
        const fn = this.getInstanceMethod(instance, method);
        return fn.apply(instance, args)
    }

    getInstance(id) {
        if (this.instance) {
            return this.instance
        } else if (this.cache.has(id)) {
            return this.cache.get(id)
        } else {
            return null
        }
    }

    getInstanceMethod(instance, method) {
        if (!this.opts.override[method]) 
            return instance[method];
        return this.opts.override[method];
    }
}

function makeDefaultFactory(Obj) {
    return (...args) => new Obj(...args);
}


function* getAllFuncs(object) {
    if (!object) return;
    yield* Object.getOwnPropertyNames(object)
    yield* getAllFuncs(Object.getPrototypeOf(object))
}