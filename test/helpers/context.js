
'use strict';

const Stream = require('stream');
const http = require('http');
const { Application: Koa } = require('../..');

module.exports = (_req, _res, app) => {
  let soc = Object.assign(new Stream.Duplex(), {
    remoteAddress: '127.0.0.1'
  })

  let req = Object.assign(new http.IncomingMessage(soc), _req)
  let res = Object.assign(new http.ServerResponse (req), _res)
  res.socket = req.socket;
  app = app || new Koa();
  return app.createContext(req, res);
};

module.exports.request = (req, res, app) => module.exports(req, res, app).request;

module.exports.response = (req, res, app) => module.exports(req, res, app).response;
