import convert from './adaptors/CoMiddlewareAdaptor';
import { respond } from './handlers/ResponseHandling';

import isGeneratorFunction from 'is-generator-function'
import compose from 'koa-compose'

import { EventEmitter } from "events";
import { format, types, inspect } from "util";
import { HttpError } from 'http-errors';
import { EndpointContext } from './adaptors/EndpointAppContext';
import { EndpointOptions } from './interfaces/EndpointOptions';

import { 
    Context  as BaseContext, 
    Request  as BaseRequest,  
    Response as BaseResponse 
} from './context';
import { handleError } from './handlers/ErrorHandler';




export class Application extends EventEmitter {
    /**
     * Make HttpError available to consumers of the library so that consumers don't
     * have a direct dependency upon `http-errors`
     */
    static HttpError = HttpError;

    #middleware: any[] = []
    endpoint: EndpointContext;
    Context: any;
    env: string;
    keys: string[];

    constructor(options: Partial<EndpointOptions> = {}) {
        super();
        this.endpoint = new EndpointContext(this, options);
        const instance = this;

        instance.Context = class Context extends BaseContext {
            static Request  = class extends BaseRequest  {};
            static Response = class extends BaseResponse {};

            public readonly app = instance;
        };

        this.env = options.env || process.env.NODE_ENV || 'development';
        if (options.keys) this.keys = options.keys;
    }


    toJSON() {
        return {
            subdomainOffset: this.endpoint.subdomainOffset,
            proxy: this.endpoint.proxy,
            env: this.env
        }
    }


    inspect() {
        return this.toJSON();
    }


    use(fn) {
        if (typeof fn !== 'function') {
            throw new TypeError('middleware must be a function!');
        }
        if (isGeneratorFunction(fn)) {
            fn = convert(fn);
        }
        this.#middleware.push(fn);
        return this;
    }


    callback() {
        const fn = compose(this.#middleware);

        if (!this.listenerCount('error')) {
            this.on('error', this.onerror);
        }

        return (req, res) => {
            const ctx = this.createContext(req, res);
            return this.handleRequest(ctx, fn);
        };;
    }

    /**
     * @api private Handle request in callback.
     */
    handleRequest(ctx, fnMiddleware) {
        const onerror = handleError.bind(ctx)
        const handleResponse = () => respond(ctx);

        ctx.bus.on("response:finished", onerror);
        return fnMiddleware(ctx).then(handleResponse).catch(onerror);
    }

    /**
     * Initialize a new context.
     */

    createContext(req, res) {
        const ctx = new this.Context(req, res, { emitFinished: true })
        ctx.res.statusCode = 404;

        this.emit("context", ctx)
        return ctx;
    }

    /**
     * Default error handler.
     */

    onerror(err) {
        if (!types.isNativeError(err))
            throw new TypeError(format('non-error thrown: %j', err));

        // @ts-ignore
        if (404 === err.status || err.expose || this.silent) return;

        const msg = err.stack || err.toString();
        console.error(`\n${msg.replace(/^/gm, '  ')}\n`);
    }

    

};

if (inspect.custom) {
    Application.prototype[inspect.custom] =
        Application.prototype.inspect;
}


/**
 * Response helper.
 */


export default Application
export { HttpError }