import { EndpointOptions, fillDefaultOptions } from '../interfaces/EndpointOptions';
import { delegate } from '../utility/delegates';
import net from "net";

export class EndpointContext {

    proxy: any;
    subdomainOffset: any;
    proxyIpHeader: any;
    maxIpsCount: any;

    constructor(public app: any, _options: Partial<EndpointOptions> = {}) {
      const options = fillDefaultOptions(_options);

      this.proxy = options.proxy;
      this.subdomainOffset = options.subdomainOffset;
      this.proxyIpHeader = options.proxyIpHeader;
      this.maxIpsCount = options.maxIpsCount;

      delegate(app, "endpoint")
        .access("proxy")
        .access("subdomainOffset")
        .access("proxyIpHeader")
        .access("maxIpsCount")
    }
  
    getRequestIps(request) {
      const proxy = this.proxy;
      const field = this.proxyIpHeader;
      const count = this.maxIpsCount
    
      if (!proxy) return []
      let ips = request.get(field).split(/\s*,\s*/)
      if (count === 0) return ips;
      return ips.slice(-count);
    }
  
    getRequestHost(request) {
      const proxy = this.proxy;
      let host = proxy && request.get('X-Forwarded-Host');
      if (!host) {
        if (request.req.httpVersionMajor >= 2) host = request.get(':authority');
        if (!host) host = request.get('Host');
      }
      if (!host) return '';
      return host.split(/\s*,\s*/, 1)[0];
    }
  
    getRequestProtocol(request) {
      if (request.socket.encrypted) return 'https';
      if (!this.proxy) return 'http';
      const proto = request.get('X-Forwarded-Proto');
      if (!proto) return 'http';
      return proto.split(/\s*,\s*/, 1)[0]
    }
  
    getRequestSubdomains(request) {
      const hostname = request.hostname;
      const offset = this.subdomainOffset;
  
      if (net.isIP(hostname)) return [];
      const pieces = hostname.split('.')
      return pieces.reverse().slice(offset);
    }
  
  }
  
  
  
  