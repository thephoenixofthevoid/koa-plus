import co from "co"

type NamedFunction = Function & { _name?: string,  name?: string  };

function* createGenerator(next) {
    return yield next()
}

function copyFunctionName(source: NamedFunction, target: NamedFunction) {
    if (source._name) return target._name = source._name;
    if (source. name) return target._name = source. name;
}

export function convert(middleware) {

    if (middleware.constructor.name !== 'GeneratorFunction') 
        throw new TypeError('middleware must be a GeneratorFunction');

    function converted(ctx, next) {
        return co.call(ctx, middleware.call(ctx, createGenerator(next)))
    }
       
    copyFunctionName(middleware, converted)
    return converted;
}

export default convert