const IP = Symbol('context#ip');

/**
 * Module dependencies.
 */

import * as util from "util";
import { ResponseHandlingContext, handleRedirect }  from './handlers/ResponseHandling'
import { delegate } from './utility/delegates';
import { ResponseHeaders } from './handlers/ResponseHeaders';
import { RequestScope } from './handlers/RequestScope';
import { LastModified } from './handlers/LastModified';
import { onFinished } from './utility/onfinished';
import contentDisposition from 'content-disposition'
import { EventEmitter } from 'events'
import url from 'url'
import contentType from 'content-type'
import typeis from 'type-is'
import path from 'path'
import { Cookies } from './utility/cookies';
import createError from 'http-errors'
const COOKIES = Symbol('context#cookies');

function defineProperties(target, source) {
  const props = Object.getOwnPropertyDescriptors(source)
  Object.defineProperties(target, props)
}

export class Context {
  public bus = new EventEmitter()
  public request = new (this.constructor as any).Request(this);
  public response = new (this.constructor as any).Response(this);
  public state = {}

  public originalUrl: any

  get cookies() {
      if (this[COOKIES]) return this[COOKIES];
      this[COOKIES] = new Cookies(this.req, this.res, {
          keys: this["app"].keys,
          secure: this.request.secure
      });
      return this[COOKIES];
  }

  set cookies(_cookies) {
      this[COOKIES] = _cookies;
  }

  constructor(public req: any, public res: any, { emitFinished }) {
    this.originalUrl = req.url;

    if (emitFinished) {
      onFinished(req, (error) => this.bus.emit("request:finished", error))
      onFinished(res, (error) => this.bus.emit("response:finished", error))
    }
  }


  public assert(value: unknown, status?: number, message?: string, opts?: any) {
      if (!value) this.throw(status, message, opts);
  }

  public throw(status?: number, message?: string, opts?: any) {
      throw createError(status, message, opts);
  }

}

export class Request {
  public req: any
  public res: any
  public bus: any

  constructor(public ctx: any) {
    this.req = ctx.req;
    this.res = ctx.res;
    this.bus = ctx.bus;
  }
}


export class Response {
  public req: any
  public res: any
  public bus: any

  constructor(public ctx: any) {
    this.req = ctx.req;
    this.bus = ctx.bus;
    this.res = ctx.res;
  }
}


defineProperties(Request.prototype, RequestScope.prototype)
defineProperties(Response.prototype, ResponseHandlingContext.prototype)
defineProperties(Response.prototype, ResponseHeaders.prototype)
defineProperties(Response.prototype, LastModified.prototype)


delegate(Response.prototype, 'res')
  .getter('socket')

delegate(Request.prototype, 'req')
  .getter('socket')
  .access('url')
  .access('method')


defineProperties(Request.prototype, {
  
  get host() {
    return this.app.endpoint.getRequestHost(this);
  },

  get protocol() {
    return this.app.endpoint.getRequestProtocol(this);
  },

  get secure() {
    return 'https' === this.protocol;
  },

  get ips() {
    return this.app.endpoint.getRequestIps(this);
  },

  get subdomains() {
    return this.app.endpoint.getRequestSubdomains(this);
  },

  get origin() {
    return `${this.protocol}://${this.host}`;
  },

  get href() {
    // support: `GET http://example.com/foo`
    if (/^https?:\/\//i.test(this.originalUrl)) return this.originalUrl;
    return this.origin + this.originalUrl;
  },

  get ip() {
    if (!this[IP]) {
      this[IP] = this.ips[0] || this.socket.remoteAddress || '';
    }
    return this[IP];
  },

  set ip(_ip) {
    this[IP] = _ip;
  },

  /**
   * @public Parse the "Host" header field hostname and support X-Forwarded-Host when a proxy is enabled.
   * @return {String} hostname
   */
  get hostname() {
    return this.URL.hostname || '';
  },

  /**
   * @public Get WHATWG parsed URL.  Lazily memoized.
   * @return {URL|Object}
   */

  get URL() {
    /* istanbul ignore else */
    if (!this.memoizedURL) {
      const originalUrl = this.originalUrl || ''; // avoid undefined in template string
      try {
        this.memoizedURL = new url.URL(`${this.origin}${originalUrl}`);
      } catch (err) {
        this.memoizedURL = Object.create(null);
      }
    }
    return this.memoizedURL;
  },

  /**
   * Get the charset when present or undefined.
   */

  get charset() {
    try {
      const { parameters } = contentType.parse(this.req);
      return parameters.charset || '';
    } catch (e) {
      return '';
    }
  },

  /**
   * Return parsed Content-Length when present.
   */

  get length() {
    const len = this.get('Content-Length');
    if (len === '') return;
    return ~~len;
  },

  /**
   * Check if the incoming request contains the "Content-Type" header field and if it contains any of the given mime `type`s.
   */

  is(type, ...types) {
    return typeis(this.req, type, ...types);
  },

  /**
   * Return the request mime type void of parameters such as "charset".
   */

  get type() {
    const type = this.get('Content-Type');
    if (!type) return '';
    return type.split(';')[0];
  },

})


defineProperties(Response.prototype, {

  redirect(url, alt) {
    handleRedirect(this, url, alt);
  },
  /**
   * Set Content-Disposition header to "attachment" with optional `filename`.
   */

  attachment(filename, options) {
    if (filename) this.type = path.extname(filename);
    this.set('Content-Disposition', contentDisposition(filename, options));
  },

  /**
   * Check whether the response is one of the listed types.
   * Pretty much the same as `this.request.is()`.
   */

  is(type, ...types) {
    return typeis.is(this.type, type, ...types);
  },


  /** @type {Boolean} Checks if the request is writable. */

  get writable() {
    return isResponseWritable(this.res)
  }

});


Object.defineProperty(Context.prototype, "toJSON", {
  value: function toJSON() {
    // .toJSON() called explicitly 
    // to prevent undesired getters invokations 

    return {
      request: this.request.toJSON(),
      response: this.response.toJSON(),
      app: this.app.toJSON(),
      originalUrl: this.originalUrl,
      req: '<original node req>',
      res: '<original node res>',
      socket: '<original node socket>'
    };
  }
})

Object.defineProperty(Context.prototype, "inspect", {
  value: function inspect() {
    if (!this.app) return this;
    return this.toJSON();
  }
})

Object.defineProperty(Request.prototype, "toJSON", {
  value: function toJSON() {
    const { method, url, header } = this
    return { method, url, header }
  }
})

Object.defineProperty(Request.prototype, "inspect", {
  value: function inspect() {
    if (!this.req) return;
    const { method, url, header } = this
    return { method, url, header }
  }
})

Object.defineProperty(Response.prototype, "toJSON", {
  value: function toJSON() {
    const { status, message, header } = this
    return { status, message, header }
  }
})

Object.defineProperty(Response.prototype, "inspect", {
  value: function inspect() {
    if (!this.res) return;
    const { status, message, header, body } = this
    return { status, message, header, body }
  }
})


/**
 * Response delegation.
 */

delegate(Context.prototype, 'response')
  .method('attachment')
  .method('redirect')
  .method('remove')
  .method('vary')
  .method('has')
  .method('set')
  .method('append')
  .method('flushHeaders')
  .access('status')
  .access('message')
  .access('body')
  .access('length')
  .access('type')
  .access('lastModified')
  .access('etag')
  .getter('headerSent')
  .getter('writable');

/**
 * Request delegation.
 */
delegate(Context.prototype, 'request')
  .method('acceptsLanguages')
  .method('acceptsEncodings')
  .method('acceptsCharsets')
  .method('accepts')
  .method('get')
  .method('is')
  .access('querystring')
  .access('idempotent')
  .access('socket')
  .access('search')
  .access('method')
  .access('query')
  .access('path')
  .access('url')
  .access('accept')
  .getter('origin')
  .getter('href')
  .getter('subdomains')
  .getter('protocol')
  .getter('host')
  .getter('hostname')
  .getter('URL')
  .getter('header')
  .getter('headers')
  .getter('secure')
  .getter('stale')
  .getter('fresh')
  .getter('ips')
  .getter('ip')


 delegate(Request.prototype, 'ctx')
  .getter('originalUrl')
  .access('response')
  .access('app')

 delegate(Response.prototype, 'ctx')
  .access('request')
  .access('app')




defineProperties(Context.prototype, {
  invoke: function(name, ...args) {
    const fn = this.app.named.get(name)
    if (fn) return fn.call(this, ...args)
  }
})

/* istanbul ignore else */
if (util.inspect.custom) {
  Context.prototype[util.inspect.custom] = Context.prototype["inspect"];
  Request.prototype[util.inspect.custom] = Request.prototype["inspect"];
  Response.prototype[util.inspect.custom] = Response.prototype["inspect"];
}



/**
 * @see https://stackoverflow.com/questions/16254385/undocumented-response-finished-in-node-js
 * @see https://github.com/nodejs/node/blob/v4.4.7/lib/_http_server.js#L486
 */

function isResponseWritable(res) {
  if (res.writableEnded) return false; // since Node > 12.9
  if (res.finished) return false; // before (undocumented)

  // pending outgoing res, but still writable

  // Tests for the existence of the socket as node sometimes does not set it.
  if (!res.socket) return true;
  return res.socket.writable;
}



