
/**
 * @name ApplicationOptions
 * 
 * @param env             Environment
 * @param keys            Signed cookie keys
 * @param proxy           Trust proxy headers
 * @param subdomainOffset Subdomain offset
 * @param proxyIpHeader   proxy ip header, default to X-Forwarded-For
 * @param maxIpsCount     max ips read from proxy ip header, default to 0 (means infinity)
 */

export interface EndpointOptions {
    proxy: boolean;
    subdomainOffset: number;
    proxyIpHeader: string;
    maxIpsCount: number;
    env: string;
    keys: string[];
    silent: boolean;
}


export function fillDefaultOptions(options: Partial<EndpointOptions>): EndpointOptions {
    return {
        proxy: false,
        subdomainOffset: 2,
        proxyIpHeader: 'X-Forwarded-For',
        maxIpsCount: 0,
        env: process.env.NODE_ENV || 'development',
        keys: [],
        silent: false,
        ...options
    }
}

