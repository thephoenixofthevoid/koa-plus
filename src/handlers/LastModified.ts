/**
 * Set the Last-Modified date using a string or a Date.
 */
export class LastModified {

    set: any;
    get: any;

    set lastModified(val: string | Date | undefined) {
        if ('string' === typeof val) val = new Date(val);
        if (val) this.set('Last-Modified', val.toUTCString());
    }

    get lastModified(): string | Date | undefined {
        const date = this.get('last-modified');
        if (date) return new Date(date);
    }

    set etag(val) {
        if (!/^(W\/)?"/.test(val)) val = `"${val}"`;
        this.set('ETag', val);
    }

    get etag() {
        return this.get('ETag');
    }

}