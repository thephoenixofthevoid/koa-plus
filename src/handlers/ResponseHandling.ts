import { handleError } from './ErrorHandler';
import escapehtml from 'escape-html'
import encodeUrl from 'encodeurl'
import destroy from 'destroy'
import Stream from 'stream'
import statuses from 'statuses'
import mimeTypes from 'mime-types'
import assert from 'assert'
import { LRU } from '../utility/LRU';

getType.LRU = new LRU(100);
function getType(type) {
    let mimeType = getType.LRU.get(type);
    if (!mimeType) {
        mimeType = mimeTypes.contentType(type);
        getType.LRU.set(type, mimeType);
    }
    return mimeType;
}

const BODY = Symbol('context#body');


function respondNull(response) {
    const rdWithMsg = response.req.httpVersionMajor < 2 && response.message;
    const body: string = rdWithMsg ? response.message : String(response.status);
    response.set('Content-Type', 'text/plain; charset=utf-8');
    response.set('Content-Length', Buffer.byteLength(body));
    return response.res.end(body);
}


export function respond(ctx) {
    if (!ctx.writable || false === ctx.respond) return; // bypassing koa
    if (statuses.empty[ctx.status]) ctx.body = null;    // ignore body
    
    const length = ctx.response.length;
    
    if (!ctx.response.res.headersSent && 
        !ctx.response.has('Content-Length') && Number.isInteger(length))
         ctx.response.set('Content-Length', length);

    if ('HEAD' === ctx.method) {
        return ctx.response.res.end();
    }
    
    // responses
    if (null === ctx.body) {
        ctx.response.remove('Content-Type');
        ctx.response.remove('Transfer-Encoding');
        return ctx.response.res.end()
    }
        
    if (null == ctx.body) return respondNull(ctx.response);
        
    if (Buffer.isBuffer(ctx.body) || 'string' === typeof ctx.body) 
        return ctx.response.res.end(ctx.response.body)
        
    if (ctx.body instanceof Stream) 
        return ctx.response.body.pipe(ctx.response.res);
        
        
    let body = JSON.stringify(ctx.response.body);
    ctx.response.set('Content-Length', Buffer.byteLength(body));
    ctx.response.res.end(body);
        
}

function setResponseBody(response, body) {

    if (null == body) {
        response[BODY] = null;
        if (!statuses.empty[response.status]) response.status = 204;
        response.remove('Content-Type');
        response.remove('Content-Length');
        response.remove('Transfer-Encoding');
        return;
    }

    if (response[BODY] === body) return;
    if (response[BODY]) response.remove('Content-Length');

    let contentType  : string|undefined = undefined;

    response[BODY] = body;
    if (!response._explicitStatus) {
        response.res.statusCode    = 200;
        if (response.req.httpVersionMajor < 2) {
            response.res.statusMessage = statuses[200];
        }
    }

    if ('string' === typeof body && /^\s*</.test(body)) {
        contentType = 'text/html; charset=utf-8';
    } else if ('string' === typeof body) {
        contentType = 'text/plain; charset=utf-8';
    } else if (Buffer.isBuffer(body)) {
        contentType = 'application/octet-stream'
    } else if (body instanceof Stream) {
        response.bus.on("response:finished", destroy.bind(null, body))
        body.once('error', handleError.bind(response.ctx));
        contentType = 'application/octet-stream'
    } else {
        response.remove('Content-Length');
        response.remove('Content-Type'); // Overwrite 
        contentType = 'application/json; charset=utf-8';
    }

    if (!response.has('Content-Type')) 
         response.set('Content-Type', contentType);
    
}


export class ResponseHandlingContext {

    [x: string]: any;
    [BODY]: any;

    get body() {
        return this[BODY]
    }

    set body(body) {
        setResponseBody(this, body)
    }

    set length(n) {
        this.set('Content-Length', n);
    }

    get length() {
        if (this.has('Content-Length')) {
            return parseInt(this.get('Content-Length'), 10) || 0;
        }
        let body = this.body;
        if (!body || body instanceof Stream) return undefined;

        if ('string' === typeof body || Buffer.isBuffer(body)) {
            return Buffer.byteLength(body);
        }
        return Buffer.byteLength(JSON.stringify(body));
    }

    get message() {
        return this.res.statusMessage || statuses[this.status];
    }

    set message(msg) {
        this.res.statusMessage = msg;
    }

    get type() {
        return (this.get('Content-Type') || '')
                    .split(';', 1)[0];
    }

    set type(type) {
        type = getType(type);
        if (type) {
            this.set('Content-Type', type);
        } else {
            this.remove('Content-Type');
        }
    }

    get status() {
        return this.res.statusCode;
    }

    set status(code) {
        if (this.headerSent) return;
        assert(Number.isInteger(code), 'status code must be a number');
        assert(code >= 100 && code <= 999, `invalid status code: ${code}`);

        this._explicitStatus = true;
        this.res.statusCode = code;
        if (this.req.httpVersionMajor < 2) this.res.statusMessage = statuses[code];
        if (this.body && statuses.empty[code]) this.body = null;
    }

}




/**
 * Perform a 302 redirect to `url`.
 *
 * The string "back" is special-cased
 * to provide Referrer support, when Referrer
 * is not present `alt` or "/" is used.
 *
 * Examples:
 *
 *    this.redirect('back');
 *    this.redirect('back', '/index.html');
 *    this.redirect('/login');
 *    this.redirect('http://google.com');
 *
 * @param {String} url
 * @param {String} [alt]
 */

export function handleRedirect(response, url, alt) {
    if ('back' === url) url = response.ctx.get('Referrer') || alt || '/';
    response.set('Location', encodeUrl(url));
  
    if (!statuses.redirect[response.status]) {
      response.status = 302;
    }
  
    if (response.ctx.accepts('html')) {
      response.type = 'text/html; charset=utf-8';
      response.body = `Redirecting to <a href="${escapehtml(url)}">${escapehtml(url)}</a>.`;
    } else {
      response.type = 'text/plain; charset=utf-8';
      response.body = `Redirecting to ${url}.`;
    }response
}