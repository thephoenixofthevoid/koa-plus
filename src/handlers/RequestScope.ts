import parse from 'parseurl'
import qs from 'querystring'
import url from 'url'
import Accepts from "accepts"
import fresh from "fresh";

import { delegate } from '../utility/delegates';

const ACCEPT = Symbol("ACCEPT");

export class RequestScope {

    req: any;
    header: any;
    method: any;
    _querycache: any;
    ctx: any;
    response: any;

    /**
     * Check if the request is idempotent.
     */

    get idempotent() {
        const methods = ['GET', 'HEAD', 'PUT', 'DELETE', 'OPTIONS', 'TRACE'];
        return !!~methods.indexOf(this.method);
    }

    public get accept() {
        if (this[ACCEPT]) return this[ACCEPT];
        this[ACCEPT] = new Accepts(this.req)
        return this[ACCEPT]
    }

    public set accept(obj) {
        this[ACCEPT] = obj;
    }

    get path() {
        return parse(this.req).pathname;
    }

    set path(path) {
        const u = parse(this.req);
        if (u.pathname === path) return;

        u.pathname = path;
        u.path = null;

        this.req.url = url.format(u);
    }

    get query() {
        const str = this.querystring;
        const c = this._querycache = this._querycache || {};
        return c[str] || (c[str] = qs.parse(str));
    }

    set query(obj) {
        this.querystring = qs.stringify(obj);
    }

    get querystring() {
        if (!this.req) return '';
        return parse(this.req).query || '';
    }

    set querystring(str) {
        const u = parse(this.req);
        if (u.search === `?${str}`) return;

        u.search = str;
        u.path = null;

        this.req.url = url.format(u);
    }

    get search() {
        if (!this.querystring) return '';
        return `?${this.querystring}`;
    }

    set search(str) {
        this.querystring = str;
    }

    get(field) {
        field = field.toLowerCase()
        if (field === 'referer' || field === 'referrer')
            return this.req.headers.referrer ||
                   this.req.headers.referer  || '';
        return this.req.headers[field] || '';
    }

    /**
     * Check if the request is fresh, aka Last-Modified and/or the ETag still match.
     */

    get fresh() {
        if ('GET' !== this.method && 'HEAD' !== this.method) return false;

        // 2xx or 304 as per rfc2616 14.26
        const status = this.ctx.status;
        if ((status >= 200 && status < 300) || 304 === status) {
            return fresh(this.header, this.response.header);
        }

        return false;
    }

    /**
     * Check if the request is stale, aka "Last-Modified" and / or the "ETag" for the
     * resource has changed.
     */
    get stale() {
        return !this.fresh;
    }

}

delegate(RequestScope.prototype, "accept")
    .method("accepts"         , "types"    )
    .method("acceptsEncodings", "encodings")
    .method("acceptsCharsets" , "charsets" )
    .method("acceptsLanguages", "languages")

delegate(RequestScope.prototype, 'req')
    .access('headers')
    .access('header', 'headers')