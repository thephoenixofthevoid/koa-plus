import statuses from "statuses"
import { types, format } from "util"


export function handleError(err) {
    // don't do anything if there is no error.
    // this allows you to pass `this.onerror` to node-style callbacks.
    if (null == err) return;

    err = ensureNativeError(err);
    const isResponded = this.headerSent || !this.writable;
    if (isResponded) err.headerSent = true;
    // Notice the main app of an issue
    this.app.emit('error', err, this);
    if (!isResponded) respondWithError(this, err)
}

function respondWithError(ctx, err) {
    replaceCookies(ctx, err.headers) 
     
     ctx.type = 'text';     // force text/plain
    let statusCode = extractStatusCodeFrom(err);
    // respond
    const msg = err.expose ? err.message : statuses[statusCode];
    ctx.status = err.status = statusCode;
    ctx.length = Buffer.byteLength(msg);
    ctx.res.end(msg);
}


function extractStatusCodeFrom(err) {
    let statusCode = err.status || err.statusCode;
    // ENOENT support
    if ('ENOENT' === err.code) statusCode = 404;
    // default to 500
    if ('number' !== typeof statusCode ||
      !statuses[statusCode]) statusCode = 500;
    return statusCode;
}


function replaceCookies(ctx, headers) {
    // first unset all headers
    for (let n of ctx.res.getHeaderNames())
                  ctx.res.removeHeader(n);
    // then set those specified
    ctx.set(headers);
}
  
function ensureNativeError(err) {
    if (types.isNativeError(err)) return err;
    return new Error(format('non-error thrown: %j', err));
}
  