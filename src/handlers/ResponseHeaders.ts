
import vary from 'vary'

export class ResponseHeaders {

    [x: string]: any;


    get header() {
        return this.headers;
    }

    get headers() {
        return this.res.getHeaders()
    }

    get headerSent() {
        return this.res.headersSent;
    }
    
    get(field) {
        return this.res.getHeader(field)
    }

    has(field) {
        return this.res.hasHeader(field)
    }

    remove(field) {
        if (this.headerSent) return;
        this.res.removeHeader(field);
    }

    set(field, val) {
        if (this.headerSent) return;

        if (2 === arguments.length) {
            if (Array.isArray(val)) val = val.map(v => typeof v === 'string' ? v : String(v));
            else if (typeof val !== 'string') val = String(val);
            this.res.setHeader(field, val);
        } else {
            for (const key in field) {
                this.set(key, field[key]);
            }
        }
    }

    append(field, val) {
        if (this.headerSent) return;
        let prev = this.get(field) || [];
        if (!Array.isArray(prev)) prev = [ prev ];
        this.res.setHeader(field, prev.concat(val));
    }

    vary(field) {
        if (this.headerSent) return;
        vary(this.res, field);
    }

    flushHeaders() {
        this.res.flushHeaders();
    }

}