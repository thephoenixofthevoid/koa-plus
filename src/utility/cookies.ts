/*!
 * cookies
 * Copyright(c) 2014 Jed Schmidt, http://jed.is/
 * Copyright(c) 2015-2016 Douglas Christopher Wilson
 * MIT Licensed
 */
import Keygrip from 'keygrip';

/**
 * RegExp to match field-content in RFC 7230 sec 3.2
 *
 * field-content = field-vchar [ 1*( SP / HTAB ) field-vchar ]
 * field-vchar   = VCHAR / obs-text
 * obs-text      = %x80-FF
 */

const RE_FIELD_CONTENT = /^[\u0009\u0020-\u007e\u0080-\u00ff]+$/;

/**
 * RegExp to match Same-Site cookie attribute value.
 */
const RE_SAME_SITE_VALUE = /^(?:lax|none|strict)$/i


export class Cookies {
  
  private keys: any;
  private secure: boolean;
  
  constructor(private request: any, private response: any, options) {
    if (options && Array.isArray(options.keys)) {
      this.keys = new Keygrip(options.keys)
    } else if (options) {
      this.keys = options.keys
    }

    if (options && options.secure !== undefined) {
      this.secure = !!options.secure
    } else {
      this.secure = request.protocol === 'https' || request.connection.encrypted
    }
  }

  public get(name, opts) {
    const options = { signed: !!this.keys, ...opts }
    const sigName = `${name}.sig`

    const header = this.request.headers["cookie"]
    if (!header) return

    const match = header.match(getPattern(name))
    if (!match) return

    const value = match[1]
    if (!options.signed) return value

    const remote = this.get(sigName, { signed: false })
    if (!remote) return

    const data = `${name}=${value}`
    if (!this.keys) throw new Error('.keys required for signed cookies');

    const index = this.keys.index(data, remote)

    if (index < 0) this.set(sigName, null, {path: "/", signed: false })  
    if (index > 0) this.set(sigName, this.keys.sign(data), { signed: false })
    
    if (index >= 0) return value
  }

  public set(name, value, opts) {
    const options = { signed: !!this.keys, secure: this.secure, ...opts }
    let headers = this.response.getHeader("Set-Cookie") || [];
    const cookie = new Cookie(name, value, opts);

    if (typeof headers == "string") headers = [headers]

    if (!this.secure && options.secure) 
      throw new Error('Cannot send secure cookie over unencrypted connection')
    if (!this.keys && options.signed) 
      throw new Error('.keys required for signed cookies')
    
    cookie.secure = options.secure
    pushCookie(headers, cookie)

    if (options.signed) {
      cookie.value = this.keys.sign(cookie.toString())
      cookie.name += ".sig"
      pushCookie(headers, cookie)
    }
    this.response.setHeader('Set-Cookie', headers)
    return this
  }
}


interface ICookie {
  name?: string;
  value?: string;
  path?: string;
  expires?: Date|null;
  domain?: string;
  httpOnly?: boolean;
  sameSite?: boolean|string;
  secure?: boolean;
  overwrite?: boolean;
  maxAge?: number;
}

interface Cookie extends ICookie {
  toString(): string;
  toHeader(): string;
}

class Cookie  {
  constructor(public name?: string, public value?: string, {
      path = "/", 
      expires = null, 
      domain = undefined, 
      httpOnly = true, 
      sameSite = false,
      secure = false, 
      overwrite = false, 
      maxAge = undefined
    } = {}) {

    Cookie.validate(name, value, { path, domain, sameSite })

    this.path      = path;
    this.domain    = domain;
    this.httpOnly  = httpOnly;
    this.sameSite  = sameSite;
    this.secure    = secure;
    this.overwrite = overwrite;

    if (value) {
      this.expires = expires
      this.maxAge  = maxAge
    } else {
      this.expires = new Date(0)
      this.maxAge  = undefined
    }
  }

  toString() {
    return `${this.name}=${this.value}`;
  }

  toHeader() {
    let header = this.toString();

    if (this.maxAge) this.expires = new Date(Date.now() + this.maxAge);

    if (this.path     ) header += `; path=${this.path}`
    if (this.expires  ) header += `; expires=${this.expires.toUTCString()}`
    if (this.domain   ) header += `; domain=${this.domain}`
    if (this.sameSite ) header += `; samesite=${this.sameSite === true ? 'strict' : this.sameSite.toLowerCase()}`
    if (this.secure   ) header += "; secure"
    if (this.httpOnly ) header += "; httponly"

    return header
  }

  static validate(name, value, { path, domain, sameSite }: ICookie = {}) {
    if (!RE_FIELD_CONTENT.test(name))
      throw new TypeError('argument name is invalid');

    if (value && !RE_FIELD_CONTENT.test(value))
      throw new TypeError('argument value is invalid');

    if (path && !RE_FIELD_CONTENT.test(path))
      throw new TypeError('option path is invalid');

    if (domain && !RE_FIELD_CONTENT.test(domain))
      throw new TypeError('option domain is invalid');

    if (sameSite && sameSite !== true && !RE_SAME_SITE_VALUE.test(sameSite))
      throw new TypeError('option sameSite is invalid')
  }
}


getPattern.cache = new Map()

function getPattern(name) {
    const cache = getPattern.cache;
    if (!cache.has(name)) cache.set(name, new RegExp(
        "(?:^|;) *" + name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, "\\$&") + "=([^;]*)"
    ))
    return cache.get(name)
}


function pushCookie(headers, cookie) {
  if (cookie.overwrite) {
    for (var i = headers.length - 1; i >= 0; i--) {
      if (headers[i].indexOf(cookie.name + '=') === 0) {
        headers.splice(i, 1)
      }
    }
  }

  headers.push(cookie.toHeader())
}
