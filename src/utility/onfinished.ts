/*!
 * on-finished
 * Copyright(c) 2013 Jonathan Ong
 * Copyright(c) 2014 Douglas Christopher Wilson
 * Copyright(c) 2020 Phoenix of the Void
 * MIT Licensed
 */

import first from "ee-first";


const ON_FINISHED = Symbol("[ON_FINISHED]")

/**
 * Determine if message is already finished.
 *
 * @param {object} msg
 * @return {boolean}
 * @public
 */

export function isFinished(msg) {
    var socket = msg.socket

    if (typeof msg.finished === 'boolean') {
        // OutgoingMessage
        return Boolean(msg.finished || (socket && !socket.writable))
    }

    if (typeof msg.complete === 'boolean') {
        // IncomingMessage
        return Boolean(msg.upgrade || !socket || !socket.readable || (msg.complete && !msg.readable))
    }

    // don't know
    return undefined
}


/**
 * Invoke callback when the response has finished, useful for
 * cleaning up resources afterwards.
 *
 * @param {object} msg
 * @param {function} listener
 * @return {object}
 * @public
 */

export function onFinished(msg, listener) {
    if (isFinished(msg) !== false) {
        setImmediate(listener, null, msg)
        return msg
    }
    
    if (msg[ON_FINISHED]) {
        msg[ON_FINISHED].push(listener)
        return msg
    }
    
    msg[ON_FINISHED] = [ listener ]
    const eeMsg = first([[msg, 'end', 'finish']], onFinish)
    msg[ON_FINISHED].unshift(() => eeMsg.cancel())

    withSocket(msg, onSocket)
    return msg

    function onSocket(socket) {
        if (!msg[ON_FINISHED]) return
        // finished on first socket event
        const eeSocket = first([[socket, 'error', 'close']], onFinish)
        msg[ON_FINISHED].unshift(() => eeSocket.cancel())
    }

    function onFinish(error) {
        const queue = msg[ON_FINISHED];
        delete msg[ON_FINISHED];
        for (let fn of queue) fn(error, msg);
    }
}

onFinished.isFinished = isFinished;

function withSocket(msg, onSocket) {
    if (msg.socket) {
        onSocket(msg.socket)
    } else {
        msg.once('socket', onSocket)
    }
}