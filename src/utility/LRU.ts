
export class LRU {

    private A = new Map();
    private B = new Map();

    constructor(private max) {}
  
    get(key) {
      let item = this.A.get(key);
      if (!item) {
        item = this.B.get(key);
        if (item) this.set(key, item);
      }
      return item;
    }
  
    set(key, value) {
        this.A.set(key, value);
        if (this.A.size < this.max) return;
        this.B = this.A;
        this.A = new Map();
    }
}