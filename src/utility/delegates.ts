export function delegate(proto, target) {

    return {

        method(name, prop = name) {
            Object.defineProperty(proto, name, {
                value: function() {
                    return this[target][prop].apply(this[target], arguments);
                },
                configurable: true
            })
            return this;
        },

        access(name, prop = name) {
            Object.defineProperty(proto, name, {
                get: function() {
                    return this[target][prop];
                },
                set: function(val) {
                    this[target][prop] = val;
                },
                configurable: true
            })
            return this
        },

        getter(name, prop = name) {
            Object.defineProperty(proto, name, {
                get: function() {
                    return this[target][prop];
                },
                configurable: true
            })
            return this;
        },

        setter(name, prop = name) {
            Object.defineProperty(proto, name, {
                set: function(val) {
                    this[target][prop] = val;
                },
                configurable: true
            })
            return this;
        }

    }
}


