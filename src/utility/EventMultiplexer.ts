import { EventEmitter } from 'events';

export const OBJECT_ADDED = Symbol('OBJECT_ADDED');
export const OBJECT_REMOVED = Symbol('OBJECT_REMOVED');


function handleRemoveListener(ev_name, listener) {
    this._listening_on.get(ev_name).delete(listener);

    if (this._listening_on.get(ev_name).size === 0) {
        for (const obj of this._objts) {
            const link = this._links.get(ev_name);
            obj.off(ev_name, link);
        }
        this._listening_on.delete(ev_name);
    }
}


function createListener(event:string|symbol) {
    const mpx = this;

    return function listener(...args) {
        mpx.emit(event, this, ...args)
    }
}



function onNewListener(ev_name, listener) {
    if (ev_name === 'removeListener' || 
        ev_name === 'newListener') return;
    
    if (!this._listening_on.has(ev_name)) {
        this._listening_on.set(ev_name, new Set());
        const link = createListener(ev_name)
        this._links.set(ev_name, link)

        for (const obj of this._objts)
            obj.on(ev_name, link);
    }
    this._listening_on.get(ev_name).add(listener);
}



export class EventMultiplexer extends EventEmitter {

    _listening_on = new Map();
    _listener_map = new Set();

    _objts = new Set();
    _links = new Map();

    constructor() {
        super();
        this.on('removeListener', handleRemoveListener);
        this.on('newListener', onNewListener);
    }

    add(...many) {
        many.forEach(addOne.bind(this))
    }

    remove(...many) {
        many.forEach(removeOne.bind(this))
    }
}


function addOne(obj) {
    if (this._objts.has(obj)) return;
    this._objts.add(obj);

    for (const ev_name of this._listening_on.keys()) {
        const link = this._links.get(ev_name);
        obj.on(ev_name, link);
    }

    this.emit(OBJECT_ADDED, obj);
}

function removeOne(obj) {
    if (!this._objts.has(obj)) return;
    this._objts.delete(obj);

    for (const ev_name of this._listening_on.keys()) {
        const link = this._links.get(ev_name);
        obj.off(ev_name, link);
    }

    this.emit(OBJECT_REMOVED, obj);
}